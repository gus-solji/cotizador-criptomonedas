import React, { Component } from 'react';
import Axios from 'axios';
import Criptomoneda from './Criptomoneda';
import Error from './Error';

class Formulario extends Component {
    state = { 
        criptomonedas: [],
        moneda:'',
        criptomoneda:'',
        error:false
     }

    async componentDidMount(){
        const url = `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD`;

    await Axios.get(url)
        .then(respuesta => {
            this.setState({
                criptomonedas: respuesta.data.Data
            })
        })

    }

    //se ejecuta cada que el usario elige una opcion del select
    obtenerValor = e =>{
        const {name,value} =e.target;
        this.setState({
            [name]:value
        })
    }

    //validar que el usuario elija las monedas
    cotizarMoneda = e =>{
        e.preventDefault();
       
        const {moneda, criptomoneda} = this.state;
        //validar que haya algo en el state
        if(moneda === '' || criptomoneda === ''){
            this.setState({
                error:true
            }, () =>{
                setTimeout(() => {
                    this.setState({
                        error:false
                    })
                },3000);
            });

            return;
        }

        //crear el objeto
        const cotizacion ={
            moneda,
            criptomoneda
        }

        //enviar los datos al componente App.js para cotizar
        this.props.cotizarCriptomoneda(cotizacion);
    }

    render() { 

        const mensaje = (this.state.error) ? <Error mensaje="Ambos campos son obligatorios"></Error> :'';
        return ( 
            <form onSubmit={this.cotizarMoneda}>
              {mensaje}
                <div className="row">
                    <label>Elige tu Moneda</label>
                    <select onChange={this.obtenerValor}
                        name="moneda"
                        className="u-full-width">
                            <option value="">Elige tu moneda</option>
                            <option value="USD">Dolar Estadounidense</option>
                            <option value="CRC">Colón costarricense</option>
                            <option value="GBP">Libras</option>
                            <option value="EUR">Euros</option>
                    </select>
                </div>

                <div className="row">
                <div>
                    <label>Elige tu Criptomoneda</label>
                    <select onChange={this.obtenerValor} name="criptomoneda" className="u-full-width">
                        <option value="">Elige tu criptomoneda</option>
                        {Object.keys(this.state.criptomonedas).map(key => (
                            <Criptomoneda key={key} criptomoneda={this.state.criptomonedas[key]}></Criptomoneda>
                        
                        ))}
                    </select>
                </div>
                </div>
                <input className="button-primary u-full-width" type="submit" value="Cotizar" />
            </form>
         );
    }
}
 
export default Formulario;