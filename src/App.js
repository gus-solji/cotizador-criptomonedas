import React, { Component } from 'react';
import imagen from './cryptomonedas.png';
import Formulario from './componentes/Formulario';
import Axios from 'axios';
import Resultado from './componentes/Resultado';
import Spinner from './componentes/Spinner';


class App extends Component {

  state = {
    resultado:{},
    monedadSeleccionada:'',
    criptomonedaSeleccionada: '',
    cargando: false
  }
  
  cotizarCriptomoneda = async (cotizacion) => {
    
    //obtener los valores
    const {moneda,criptomoneda} = cotizacion;

    //realizar consulta con axios a la api
    const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptomoneda}&tsyms=${moneda}`;

    await Axios.get(url)
      .then(respuesta => {
        this.setState({
          resultado: respuesta.data.DISPLAY[criptomoneda][moneda],
          cargando:true
        }, () => {
          //3 segundos despues cambia a false
          setTimeout(() =>{
            this.setState({
              cargando:false
            })
          },3000);
        })
       
      });

  }

  render() {

    const resultado = (this.state.cargando) ?  <Spinner></Spinner> : <Resultado resultado={this.state.resultado}></Resultado>;

    return (
      <div className="container">
        <div className="row">
            <div className="one-half column">
                <img src={imagen} alt="imagen" className="logotipo"></img>
            </div>
            <div className="one-half column">
                <h1>Cotiza Criptomonedas al instante</h1>
                <Formulario cotizarCriptomoneda={this.cotizarCriptomoneda}></Formulario>
                
                {resultado}
            </div>
            
        </div>
      </div>
    );
  }
}

export default App;
